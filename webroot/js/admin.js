// Call the dataTables jQuery plugin
$(document).ready(function() {
	
	$('input.price').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0', aSign: '', lZero: 'deny'});
	$('.price').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0', aSign: '$ ', lZero: 'deny'});


	$.fn.dataTable.moment( 'DD/MM/YYYY HH:mm' );
	$('.datatable').DataTable({
	  dom: '<"float-left" B>frtip',
	  buttons: [
	      'excel'
	  ],
	  pageLength: 50,
	  lengthChange: false,
	      // searching: false,
	      info:     false,
	      language: {
	          url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json',              
	      },
	  "order": [[ 0, 'asc' ]],
	  buttons: [
	      'excel', 'pdf'
	  ],
	});


	$('input.date').datepicker({
		language: "es"
	});
});