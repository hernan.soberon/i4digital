<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HealthsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HealthsTable Test Case
 */
class HealthsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HealthsTable
     */
    public $Healths;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Healths',
        'app.Profiles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Healths') ? [] : ['className' => HealthsTable::class];
        $this->Healths = TableRegistry::getTableLocator()->get('Healths', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Healths);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
