<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;


/**
 * Profiles Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\HealthsTable&\Cake\ORM\Association\BelongsTo $Healths
 * @property \App\Model\Table\LayoffsTable&\Cake\ORM\Association\BelongsTo $Layoffs
 * @property \App\Model\Table\PensionsTable&\Cake\ORM\Association\BelongsTo $Pensions
 *
 * @method \App\Model\Entity\Profile get($primaryKey, $options = [])
 * @method \App\Model\Entity\Profile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Profile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Profile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Profile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Profile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Profile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Profile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProfilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('profiles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Healths', [
            'foreignKey' => 'health_id',
        ]);
        $this->belongsTo('Layoffs', [
            'foreignKey' => 'layoff_id',
        ]);
        $this->belongsTo('Pensions', [
            'foreignKey' => 'pension_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('doc')
            ->maxLength('doc', 255)
            ->requirePresence('doc', 'create')
            ->notEmptyString('doc');

        $validator
            ->scalar('position')
            ->maxLength('position', 255)
            ->requirePresence('position', 'create')
            ->notEmptyString('position');

        $validator
            ->scalar('career')
            ->maxLength('career', 255)
            ->allowEmptyString('career');

        $validator
            ->scalar('salary')
            ->maxLength('salary', 255)
            ->allowEmptyString('salary');

        $validator
            ->scalar('retired')
            ->maxLength('retired', 255)
            ->allowEmptyString('retired');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['health_id'], 'Healths'));
        $rules->add($rules->existsIn(['layoff_id'], 'Layoffs'));
        $rules->add($rules->existsIn(['pension_id'], 'Pensions'));

        return $rules;
    }


    /**
     * Fix the date before save it into DB
     */
    public function beforeSave(Event $event, EntityInterface $entity)
    {
        if (isset($entity->retired) && $entity->retired){
            $entity->retired = date_create_from_format('d/m/Y', $entity->retired)->format('Y-m-d'); 
        }
       if (isset($entity->salary)) {
            $entity->salary = trim(preg_replace('/[^0-9]/', '', $entity->salary)); ;
        }
    }
}
