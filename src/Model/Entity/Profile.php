<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Profile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $doc
 * @property string $position
 * @property string|null $career
 * @property int|null $health_id
 * @property int|null $layoff_id
 * @property int|null $pension_id
 * @property string|null $salary
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $retired
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Health $health
 * @property \App\Model\Entity\Layoff $layoff
 * @property \App\Model\Entity\Pension $pension
 */
class Profile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'first_name' => true,
        'last_name' => true,
        'doc' => true,
        'position' => true,
        'career' => true,
        'health_id' => true,
        'layoff_id' => true,
        'pension_id' => true,
        'salary' => true,
        'created' => true,
        'modified' => true,
        'retired' => true,
        'user' => true,
        'health' => true,
        'layoff' => true,
        'pension' => true,
    ];

    public function _getName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function _getDate()
    {
        return $this->created->format('d/m/Y');
    }

    public function _getExitdate()
    {
        return ($this->retired) ? $this->retired->format('d/m/Y') : '';
    }
}
