<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Layoffs Controller
 *
 * @property \App\Model\Table\LayoffsTable $Layoffs
 *
 * @method \App\Model\Entity\Layoff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LayoffsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $layoffs = $this->paginate($this->Layoffs);

        $this->set(compact('layoffs'));
    }

    /**
     * View method
     *
     * @param string|null $id Layoff id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $layoff = $this->Layoffs->get($id, [
            'contain' => ['Profiles'],
        ]);

        $this->set('layoff', $layoff);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $layoff = $this->Layoffs->newEntity();
        if ($this->request->is('post')) {
            $layoff = $this->Layoffs->patchEntity($layoff, $this->request->getData());
            if ($this->Layoffs->save($layoff)) {
                $this->Flash->success(__('The layoff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The layoff could not be saved. Please, try again.'));
        }
        $this->set(compact('layoff'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Layoff id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $layoff = $this->Layoffs->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $layoff = $this->Layoffs->patchEntity($layoff, $this->request->getData());
            if ($this->Layoffs->save($layoff)) {
                $this->Flash->success(__('The layoff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The layoff could not be saved. Please, try again.'));
        }
        $this->set(compact('layoff'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Layoff id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $layoff = $this->Layoffs->get($id);
        if ($this->Layoffs->delete($layoff)) {
            $this->Flash->success(__('The layoff has been deleted.'));
        } else {
            $this->Flash->error(__('The layoff could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
