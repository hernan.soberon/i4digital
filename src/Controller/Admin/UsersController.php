<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout', 'add']);

        $menu_left_active = 'users';
        $this->set(compact('menu_left_active'));
        
    }


    /**
     * Login method
     */
    public function login()
    {
        $this->viewBuilder()->setLayout('simple');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Usuario y contraseña incorrectos'));
        }
    }


    /**
     *  Logout method 
     */
    public function logout()
    {
        // $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }


    



    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $users = $this->Users->find('all', [
            'contain' => ['Profiles']
        ]);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Profiles' => ['Healths', 'Pensions', 'Layoffs']],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        
        $user = $this->Users->newEntity(null, ['associated' => ['Profiles']]);

        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['associated' => ['Profiles']]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Nuevo empleado añadido'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No fue posible crear el empleado. Valida los datos'));

        }

        $healths = $this->Users->Profiles->Healths->find('list', ['order' => ['title' => 'ASC']]);
        $layoffs = $this->Users->Profiles->Layoffs->find('list', ['order' => ['title' => 'ASC']]);
        $pensions = $this->Users->Profiles->Pensions->find('list', ['order' => ['title' => 'ASC']]);

        $this->set(compact('user', 'healths', 'layoffs', 'pensions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Profiles'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        
        $healths = $this->Users->Profiles->Healths->find('list', ['order' => ['title' => 'ASC']]);
        $layoffs = $this->Users->Profiles->Layoffs->find('list', ['order' => ['title' => 'ASC']]);
        $pensions = $this->Users->Profiles->Pensions->find('list', ['order' => ['title' => 'ASC']]);

        $this->set(compact('user', 'healths', 'layoffs', 'pensions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->warning(__('Empleado eliminado'));
        } else {
            $this->Flash->error(__('No fue posible borrar este empleado'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
