<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Pensions Controller
 *
 * @property \App\Model\Table\PensionsTable $Pensions
 *
 * @method \App\Model\Entity\Pension[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PensionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $pensions = $this->paginate($this->Pensions);

        $this->set(compact('pensions'));
    }

    /**
     * View method
     *
     * @param string|null $id Pension id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pension = $this->Pensions->get($id, [
            'contain' => ['Profiles'],
        ]);

        $this->set('pension', $pension);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pension = $this->Pensions->newEntity();
        if ($this->request->is('post')) {
            $pension = $this->Pensions->patchEntity($pension, $this->request->getData());
            if ($this->Pensions->save($pension)) {
                $this->Flash->success(__('The pension has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pension could not be saved. Please, try again.'));
        }
        $this->set(compact('pension'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pension id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pension = $this->Pensions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pension = $this->Pensions->patchEntity($pension, $this->request->getData());
            if ($this->Pensions->save($pension)) {
                $this->Flash->success(__('The pension has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pension could not be saved. Please, try again.'));
        }
        $this->set(compact('pension'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pension id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pension = $this->Pensions->get($id);
        if ($this->Pensions->delete($pension)) {
            $this->Flash->success(__('The pension has been deleted.'));
        } else {
            $this->Flash->error(__('The pension could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
