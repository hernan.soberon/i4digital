<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>


<div class="users index large-9 medium-8 columns content">
    
    <div class="float-right">
        <?= $this->Html->link('<i class="fa fa-plus-circle"></i>'.__(' Añadir Empleado'), 
                        ['action' => 'add'], 
                        ['escape' => false, 'class' => 'btn btn-success']); ?>
    </div>
    <h1 class="h3 mb-4 text-gray-800">Empleados</h1>
    
    <table class="table table-responsive datatable">
        <thead>
            <tr>
                <th>#</th>
                <th>Documento</th>
                <th>Nombre Completo</th>
                <th>Usuario</th>
                <th>Correo</th>
                <th>Cargo</th>
                <th>Salario</th>
                <th>Fecha de Ingreso</th>
                <th>Fecha de Retiro</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->id) ?></td>
                <td><?= h($user->profile->doc) ?></td>
                <td><?= h($user->profile->name) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->profile->position) ?></td>
                <td class="price"><?= floatval(h($user->profile->salary)) ?></td>
                <td><?= h($user->profile->date) ?></td>
                <td><?= h($user->profile->exitdate) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $user->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-success btn-xs']) ?>
                    <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['action' => 'delete', $user->id], 
                                ['confirm' => __('Está seguro de borrar el usuario: {0}?', h($user->username)), 
                                 'escape' => false, 'class' => 'btn btn-danger btn-xs']); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
