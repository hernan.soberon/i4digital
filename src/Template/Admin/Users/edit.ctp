<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="card shadow mb-4">
    <div class="card-body">
        <?= $this->Form->create($user) ?>
        <legend><?= __('Editar empleado').': '.$user->profile->name ?></legend>
        <div class="row">
            <div class="col-lg-6">
                <fieldset>
                    <?php
                        echo $this->Form->control('profile.first_name', [
                            'class' => 'form-control', 
                            'label' => __('Nombres'), 
                        ]);
                        echo $this->Form->control('profile.last_name', [
                            'class' => 'form-control', 
                            'label' => __('Apellidos'), 
                        ]);
                        echo $this->Form->control('profile.doc', [
                            'class' => 'form-control', 
                            'label' => __('Número de Documento'), 
                        ]);

                        echo $this->Form->control('email', [
                            'class' => 'form-control', 
                            'label' => __('Correo Electrónico'), 
                        ]);
                        echo $this->Form->control('username', [
                            'class' => 'form-control', 
                            'label' => __('Nombre de Usuario'), 
                        ]);
                        echo $this->Form->control('profile.retired', [
                            'class' => 'form-control date', 
                            'type' => 'text',
                            'value' => (($user->profile->retired) ? $user->profile->retired->format('d/m/Y') : ''),
                            'label' => __('Fecha de retiro'), 
                        ]);
                    ?>
                </fieldset>        
            </div>
            <div class="col-lg-6">
                <fieldset>
                    <?php
                        echo $this->Form->control('profile.position', [
                            'class' => 'form-control', 
                            'label' => __('Cargo'), 
                        ]);

                        echo $this->Form->control('profile.career', [
                            'class' => 'form-control', 
                            'label' => __('Profesión'), 
                        ]);

                        echo $this->Form->control('profile.salary', [
                            'class' => 'form-control price', 
                            'label' => __('Salario'), 
                        ]);

                        echo $this->Form->control('profile.health_id', [
                            'class' => 'form-control', 
                            'options' => $healths,
                            'label' => __('Entidad afiliada en Salud'), 
                        ]);

                        echo $this->Form->control('profile.layoff_id', [
                            'class' => 'form-control', 
                            'options' => $layoffs,
                            'label' => __('Entidad afiliada en Cesantias'), 
                        ]);

                        echo $this->Form->control('profile.pension_id', [
                            'class' => 'form-control', 
                            'options' => $pensions,
                            'label' => __('Entidad afiliada en Pensiones'), 
                        ]);
                    ?>
                </fieldset>        
            </div>
        </div>
        
        <hr> 
        <?= $this->Html->link('Volver', ['action' => 'index'], ['class' => 'btn btn-default']); ?>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>