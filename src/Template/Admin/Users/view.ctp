<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="users">
    <h3><?= h($user->profile->name) ?></h3>
    <div class="row">
        <div class="col-lg-6">
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Correo') ?></th>
                    <td><?= h($user->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Usuario') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Documento') ?></th>
                    <td><?= $this->Number->format($user->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Ingresó el día') ?></th>
                    <td><?= h($user->profile->date) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Retirado el día') ?></th>
                    <td><?= h($user->profile->exitdate) ?></td>
                </tr>
            </table>        
        </div>
        <div class="col-lg-6">
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Nombres') ?></th>
                    <td><?= h($user->profile->first_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Apellidos') ?></th>
                    <td><?= h($user->profile->last_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Profesión') ?></th>
                    <td><?= h($user->profile->career) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Cargo') ?></th>
                    <td><?= h($user->profile->position) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Salario') ?></th>
                    <td class="price"><?= h($user->profile->salary) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('EPS') ?></th>
                    <td><?= h($user->profile->health->title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Pensiones') ?></th>
                    <td><?= h($user->profile->pension->title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Cesantías') ?></th>
                    <td><?= h($user->profile->layoff->title) ?></td>
                </tr>
            </table>        
        </div>
    </div>
    <hr>
    <?= $this->Html->link('Volver', ['action' => 'index'], ['class' => 'btn btn-default']); ?>
    <?= $this->Html->link('Editar', ['action' => 'edit', $user->id], ['class' => 'btn btn-success']); ?>
</div>
