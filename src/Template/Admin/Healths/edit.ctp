<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Health $health
 */
?>

<div class="card shadow mb-4">
    <div class="card-body">
        <?= $this->Form->create($health) ?>
        <legend><?= $health->title ?></legend>
        <div class="row">
            <div class="col-lg-6">
                <fieldset>
                    <?php
                        echo $this->Form->control('title', [
                            'class' => 'form-control', 
                            'label' => __('Nombre'), 
                        ]);
                    ?>
                </fieldset>        
            </div>
        </div>
        
        <hr> 

        <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>