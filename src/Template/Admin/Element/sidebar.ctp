<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <?= $this->Html->link('i4Digital <br> Prueba Técnica', ['controller' => 'users', 'action' => 'index'], 
              ['escape' => false, 'class' => 'sidebar-brand d-flex align-items-center justify-content-center']) ?>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item">
    <?= $this->Html->link('<i class="fas fa-fw fa-users"></i> Usuarios', ['controller' => 'users', 'action' => 'index'], 
              ['escape' => false, 'class' => 'nav-link'])  ?>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Empresas
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <?= $this->Html->link('<i class="fas fa-fw fa-user-md"></i> Salud', ['controller' => 'healths', 'action' => 'index'], 
              ['escape' => false, 'class' => 'nav-link'])  ?>
  </li>
  <li class="nav-item">
    <?= $this->Html->link('<i class="fas fa-fw fa-hospital"></i> Cesantías', ['controller' => 'layoffs', 'action' => 'index'], 
              ['escape' => false, 'class' => 'nav-link'])  ?>
  </li>
  <li class="nav-item">
    <?= $this->Html->link('<i class="fas fa-fw fa-briefcase"></i> Pensiones', ['controller' => 'pensions', 'action' => 'index'], 
              ['escape' => false, 'class' => 'nav-link'])  ?>
  </li>

 
  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->