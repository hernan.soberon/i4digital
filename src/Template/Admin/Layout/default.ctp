<?php
/**
 * i4Digital 
 * Tecnical Test
 * 
 * Emploes manager
 *
 * By Hernan Soberon (H'Soberon) @ Ganimedes 2020
 *
 * @copyright     i4digital
 * @link          https://hsoberon.com/i4digital/
 * @since         0.01
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="tecnical test for i4Digital" />
    <meta name="author" content="hsoberon" />
    <title>i4Digital - <?= $this->fetch('title') ?></title>
    

    <!-- Font Awesome icons (free version)-->
    <?= $this->Html->css('../vendor/fontawesome-free/css/all.min.css') ?>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <?= $this->Html->meta('icon') ?>

    <!-- Core theme CSS (includes Bootstrap)-->
    <?= $this->Html->css('sb-admin-2.min.css') ?>
    <?= $this->Html->css('../vendor/datatables/dataTables.bootstrap4.min') ?>
    <?= $this->Html->css('../vendor/datepicker/css/bootstrap-datepicker3.standalone.min') ?>
    <?= $this->Html->css('admin.css') ?>

    <!-- JQUERY -->
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js') ?>


    <?php 
        //this is loaded at the end of the page.
        echo $this->Html->script([
            // Bootstrap core JS
            'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js',
            // Third party plugin JS
            'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js',
            // Core theme JS
            'sb-admin-2.min.js',
            '../vendor/datatables/jquery.dataTables.min',
            '../vendor/datatables/dataTables.bootstrap4.min',
            // Moments
            '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js',
            '//cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js', //moments to datatables
            // Autonumeric
            '../vendor/autonumeric/autoNumeric-min',
            // Datepicker
            '../vendor/datepicker/js/bootstrap-datepicker.min',
            '../vendor/datepicker/locales/bootstrap-datepicker.es.min',
            // Custom JS
            'admin'
            ], ['block' => 'scriptBottom']
        );
    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
        <?= $this->Element('sidebar') ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            
            <!-- Main Content -->
            <div id="content">
            
                <?= $this->Element('topbar') ?>                

                <!-- Begin Page Content -->
                <div class="container-fluid">


                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                  

                </div>
                <!-- /.container-fluid end of page content  -->


            </div>
            <!-- End of Main Content -->

            <?= $this->Element('footer') ?>


        </div><!-- Content Wrapper -->
    </div><!-- ./page wrapper -->


    <!--Load JavaScript-->
    <?= $this->fetch('scriptBottom'); ?>    
    
</body>
</html>
