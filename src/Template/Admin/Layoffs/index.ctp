<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Health[]|\Cake\Collection\CollectionInterface $healths
 */
?>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>


<div class="content">
    
    <div class="float-right">
        <?= $this->Html->link('<i class="fa fa-plus-circle"></i>'.__(' Añadir Empresa de Cesantias'), 
                        ['action' => 'add'], 
                        ['escape' => false, 'class' => 'btn btn-success']); ?>
    </div>
    <h1 class="h3 mb-4 text-gray-800">Entidades afiliadas en Cesantias</h1>
    
    <table class="table table-responsive datatable">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($layoffs  as $company): ?>
            <tr>
                <td><?= h($company->id) ?></td>
                <td><?= h($company->title) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $company->id], ['class' => 'btn btn-success btn-sm']) ?>
                    <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['action' => 'delete', $company->id], 
                                ['confirm' => __('Está seguro de borrar la empresa: {0}?', h($company->title)), 
                                 'escape' => false, 'class' => 'btn btn-danger btn-sm']); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>


