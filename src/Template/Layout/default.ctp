<?php
/**
 * i4Digital 
 * Tecnical Test
 * 
 * Emploes manager
 *
 * By Hernan Soberon (H'Soberon) @ Ganimedes 2020
 *
 * @copyright     i4digital
 * @link          https://hsoberon.com/i4digital/
 * @since         0.01
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="tecnical test for i4Digital" />
    <meta name="author" content="hsoberon" />
    <title>i4Digital - Tecnical Test</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    
    <link href="css/styles.css" rel="stylesheet" />

    <?= $this->Html->meta('icon') ?>

    <!-- Core theme CSS (includes Bootstrap)-->
    <?= $this->Html->css('styles.css') ?>
    <?= $this->Html->css('custom.css') ?>

    <!-- JQUERY -->
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js') ?>


    <?php 
        //this is loaded at the end of the page.
        echo $this->Html->script([
            // Bootstrap core JS
            'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js',
            // Third party plugin JS
            'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js',
            // Contact form JS
            'jqBootstrapValidation.js',
            'contact_me.js',
            // Core theme JS
            'scripts'
            ], ['block' => 'scriptBottom']
        );
    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body id="page-top">
    <?= $this->Element('header') ?>
    
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

    <!--Load JavaScript-->
    <?= $this->fetch('scriptBottom'); ?>    
    
</body>
</html>
